<div class="col-sm-3 col-md-2 sidebar">
		 <div class="sidebar_top">
			 <h1>Tomasz Kulinowski</h1> 
			 <img id="fotka" src="images/5.png" alt=""/>
		 </div>
		 		
		<div class="details">
			<h3><span class="lang_pl">KONTAKT</span><span class="lang_en">CONTACT</span></h3>
			<div id="number">(+48) 506 <span>XXX XXX</span><span id="812154256" style="display:none;">727 456</span></div>
			<p><a href="mailto:toxmasz@kulinoxwski.pl?Subject=Web%20CV%20" onmouseover="this.href=this.href.replace(/x/g,'');">tomasz<img class="malpa" src="images/malpa.png" alt=""/>kulinowski.pl</a></p>
			<div class="soc">
				<a href="http://www.linkedin.com/in/tomasz-kulinowski" target="_blank">
					<img class="logo_soc" src="images/logo_linkedin.png" alt="logo_linkedin"/>
				</a>
			</div>
			<div class="soc">
				<a href="https://www.goldenline.pl/tomasz-kulinowski/" target="_blank">
					<img class="logo_soc" src="images/logo_Goldenline.png" alt="logo_Goldenline"/>
				</a>
			</div>
			<div class="soc">
				<a href="https://github.com/diabollo1" target="_blank">
					<img class="logo_soc" src="images/logo_github.png" alt="logo_github"/>
				</a>
			</div>
			<div class="soc">
				<a href="https://bitbucket.org/diabollo/" target="_blank">
					<img class="logo_soc" src="images/logo_bitbucket.png" alt="logo_bitbucket"/>
				</a>
			</div>
			<div class="soc">
				<a href="https://about.me/kulinowski" target="_blank">
					<img class="logo_soc" src="images/logo_about.me.png" alt="logo_github"/>
				</a>
			</div>
				
			<br>
			<br>
			<div class="address">
				<h3><span class="lang_pl">UMIEJĘTNOŚCI</span><span class="lang_en">SKILLS</span></h3>
				<span>Web (HTML, CSS, SQL, JavaScript/jQuery, PHP, XML)</span>
				<span class="lang_pl">Programowanie (C, C++, Java)</span><span class="lang_en">Programming (C, C ++, Java)</span>
				<span class="lang_pl">Linux (administracja, skrypty)</span><span class="lang_en">Linux (administration, scripts)</span>
				<span class="lang_pl">Windows (skrypty bat,VBS)</span><span class="lang_en">Windows (bat scripts, VBS)</span>
				<span>Microsoft Office (Visual Basic)</span>
				<span class="lang_pl">grafika (Photoshop, InDesign, Ilustrator, AutoCAD, Corel Draw, Flash)		</span><span class="lang_en">graphics (Photoshop, InDesign, Ilustrator , AutoCAD, Corel Draw, Flash)</span>
				<span>Mainframe (z/OS i JCL)</span>
				<span class="lang_pl">Serwis PC (software/hardware)</span><span class="lang_en">PC service (software / hardware)</span>
				<span class="lang_pl">Prawo jazdy kat. B</span><span class="lang_en">Driving license category B</span>
			</div>
		</div>
		<div class="clearfix"></div>
</div>

<script>
	$('#number').click(function() {
    $(this).find('span').toggle();
});
</script>