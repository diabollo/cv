<!DOCTYPE HTML>
<html lang="pl-PL">
<head>
	<?php include("head.php");?>
</head>
<body>

<!-- header -->
<?php include("header.php");?>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="content">

	<?php include("menu.php");?>

<!--DOŚWIADCZENIE ZAWODOWE================================================-->
		<div class="company">
	
	<!--=====================-->
			<h3 class="clr2"><span class="lang_pl">DOŚWIADCZENIE ZAWODOWE</span><span class="lang_en">EXPERIENCE</span></h3>
			<div class="company_details">
				<h4>PKO BP S.A.<span class="lang_pl">od 2014</span><span class="lang_en">from 2014</span></h4>
				<h6><span class="lang_pl">Specjalista ds. Monitorowania systemów informatycznych</span>
				<span class="lang_en">Monitoring of Information Systems Specialist</span></h6>
				
				<p><b>HTML, CSS, SQL, JavaScript/jQuery, PHP, XML, Java, Linux/UNIX</b></p>
					
					<div class="ul_margines">
						<ul>
							
							<li>
								<span class="lang_pl">administrowanie, utrzymanie oraz zapewnienie ciągłości funkcjonowania narzędzi monitorowania, wspierających eksploatację systemów i infrastruktury IT </span>
								<span class="lang_en">administering, maintaining and ensuring the continuity of monitoring tools supporting the operation of IT systems and infrastructure</span>
							</li>
							<li>
								<span class="lang_pl">projektowanie i budowa systemów wspierających eksploatację i zarządzanie infrastrukturą oraz usługami IT, bazami danych, middleware   </span>
								<span class="lang_en">design and construction of systems supporting the operation and management of infrastructure and IT services, databases, middleware</span>
							</li>
							<li>
								<span class="lang_pl">udział w tworzeniu procedur związanych z eksploatacją systemów informatycznych i infrastruktury, w szczególności wspieranych przez rozwijane i administrowane narzędzia</span>
								<span class="lang_en">participation in creating procedures related to the operation of IT systems and infrastructure, in particular supported by developed and administered tools</span>
							</li>
							<li>
								<span class="lang_pl">prowadzenie dokumentacji technicznej w zakresie systemów monitorowania, wykrywania sprzętu, gromadzenia danych o usługach, sprzęcie i zdarzeniach</span>
								<span class="lang_en">keeping technical documentation in the scope of monitoring systems, hardware detection, data collection about services, equipment and events</span>
							</li>
							<li>
								<span class="lang_pl">zbieranie danych i tworzenie raportów z zarządzanych i rozwijanych systemów</span>
								<span class="lang_en">collecting data and creating reports on managed and developed systems</span>
							</li>
							<li>
								<span class="lang_pl">utrzymanie ciągłości pracy systemów wsparcia eksploatacji</span>
								<span class="lang_en">maintaining continuity of operation support systems</span>
							</li>
							<li>
								<span class="lang_pl">wsparcie klientów wewnętrznych departamentu</span>
								<span class="lang_en">support for internal departments of the department</span>
							</li>
							<li>
								<span class="lang_pl">współpraca z administratorami infrastruktury i usług IT</span>
								<span class="lang_en">cooperation with infrastructure and IT services administrators</span>
							</li>	
						</ul>
					</div>
				<p class="cmpny1"></p>
			</div>
	<!--=====================-->
			<div class="company_details">
				<h4>RomDruk<span class="lang_pl">od 2013</span><span class="lang_en">from 2013</span></h4>
				<h6><span class="lang_pl">Grafik</span><span class="lang_en">Graphic Designer</span></h6>
				<!--<p>blablabla</p>-->
				<div class="ul_margines">
					<ul>
						<li><span class="lang_pl">tworzenie projektów w programach <b>InDesign, Ilustrator, Photoshop</b></span><span class="lang_en">creation of projects in <b>InDesign, Ilustrator, Photoshop</b></span></li>
						<li><span class="lang_pl">tworzenie i edycja druków dla dużych sieci hotelowych: <i>Marriott, AccorHotels (Sofitel, Mercure, Ibis), Hampton by Hilton, Orbis, Renaissance Hotels, Golden Tulip</i></span>
						<span class="lang_en">creation and edition of prints and forms for large hotel chains: <i>Marriott, AccorHotels (Sofitel, Mercure, Ibis), Hampton by Hilton, Orbis, Renaissance Hotels, Golden Tulip</i></span></li>
					</ul>
				</div>
				<!--
					<div class="ul_margines">
						<ul>
							
							<li><li>aaa</li><li>ss</li></li>
						</ul>
					</div>
				-->
				<p class="cmpny1"></p>
			</div>
	<!--=====================-->
			<div class="company_details">
				<h4>PKO BP S.A.<span>2014 - 2017</span></h4>
				<h6><span class="lang_pl">Operator IT</span><span class="lang_en">IT Operator</span></h6>
				<!--<p>blablabla</p>-->
					<div class="ul_margines">
						<ul>
							<li><span class="lang_pl">tworzenie narzędzi do monitorowania struktury teleinformatycznej banku (<b>HTML, PHP, MySQL, jQuery, XML</b>)</span>
								<span class="lang_en">creating tools for monitoring the bank teleinformation structure (<b>HTML, PHP, MySQL, jQuery, XML</b>)</span></li>
							<li><span class="lang_pl">tworzenie aplikacji okienkowych (<b>VBA</b>) i skryptów (<b>VBA, VBS</b>) do automatyzacji pracy</span>
								<span class="lang_en">creating windows applications (<b>VBA</b>) and scripts (<b>VBA, VBS</b>) for work automation</span></li>
							<li><span class="lang_pl">nadzór nad infrastrukturą informatyczną banku - <i>IBM Tivoli Integrated Portal</i></span>
								<span class="lang_en">supervising the bank IT infrastructure - <i>IBM Tivoli Integrated Portal</i></span></li>
							<li><span class="lang_pl">obsługa informatycznych systemów przetwarzania danych - <i>Sygnity Flexcube</i></span>
								<span class="lang_en">servicing of IT data processing systems - <i>Sygnity Flexcube</i></span></li>
						</ul>
					</div>
				<p class="cmpny1"></p>
			</div>
	<!--=====================-->
			<div class="company_details">
				<h4>Inteligo Financial Services S.A.<span>2013 - 2014</span></h4>
				<h6><span class="lang_pl">Operator Service Desk</span><span class="lang_en">Service Desk Operator</span></h6>
				<!--<p>blablabla</p>-->
					<div class="ul_margines">
						<ul>
							<li><span class="lang_pl">tworzenie aplikacji do automatyzacji pracy i <b>testów</b> w <b>jQuery</b></span>
								<span class="lang_en">creating applications for work automation and <b>testing</b> in <b>jQuery</b></span></li>
							<li><span class="lang_pl">tworzenie <b>skryptów shell</b> sprawdzających logi systemowe i aplikacyjne</span>
								<span class="lang_en">creating <b>shell scripts</b> checking system and application logs</span></li>
							<li><span class="lang_pl">nadzór nad infrastrukturą informatyczną banku - <i>HP Business Service Management</i></span>
								<span class="lang_en">supervising the bank IT infrastructure - <i>HP Business Service Management</i></span></li>
							</ul>
					</div>
				<p class="cmpny1"></p>
			</div>
	<!--=====================-->
			<div class="company_details">
				<h4>Fundacja Nauka i Wiedza<span>2011 - 2013</span></h4>
				<h6><span class="lang_pl">Programista Java</span><span class="lang_en">Java Programmer</span></h6>
				<!--<p>blablabla</p>-->
					<div class="ul_margines">
						<ul>
							<li><span class="lang_pl">opracowywania apletów/animacji <b>Flash</b> i <b>Java</b> oraz ich publikacja w sieci</span>
								<span class="lang_en">development of <b>Flash</b> and <b>Java</b> applets/animations and their publication on the web</span></li>
							<li><span class="lang_pl">modernizacja i tagowanie materiałów edukacyjnych na stronie fundacji <i>edukator.pl</i></span>
								<span class="lang_en">modernisation and tagging of educational materials on the foundation website <i>edukator.pl</i> </span></li>
							</ul>
					</div>
				<p class="cmpny1"></p>
			</div>
	<!--=====================-->
			<div class="company_details">
				<h4>OVB Allfinanz Polska<span>2009 - 2011</span></h4>
				<h6><span class="lang_pl">Doradca finansowy</span><span class="lang_en">Financial Adviser</span></h6>
				<!--<p>blablabla</p>-->
					<div class="ul_margines">
						<ul>
							<li><span class="lang_pl">kontak z kientem i doradztwo finansowe</span>
								<span class="lang_en">contact with the client and financial advising</span></li>
							<li><span class="lang_pl">szkolenie i przekazywanie wiedzy podległemu zespołowi</span>
								<span class="lang_en">training and transferring knowledge to the dependent team</span></li>
						</ul>
					</div>
			</div>
	<!--=====================-->
			<div class="company_details">
				<h4>Agregaty Pex-Pool Plus<span>2005 - 2013</span></h4>
				<h6><span class="lang_pl">Specjalista IT</span><span class="lang_en">IT Specialist</span></h6>
				<!--<p>blablabla</p>-->
					<div class="ul_margines">
						<ul>
							<li><span class="lang_pl">nadzór nad siecią (współdzielenie folderów, drukarki sieciowe)</span>
								<span class="lang_en">network supervision (folder sharing, network printers)</span></li>
							<li><span class="lang_pl">dobór, konfiguracja, konserwacja i naprawy sprzętu</span>
								<span class="lang_en">selection, configuration, maintenance and repair of the equipment</span></li>
							</ul>
					</div>
			</div>
	<!--=====================-->
		
		
		</div>
<!--===========================================================-->

<!--WYKSZTAŁCENIE================================================-->
		<div class="skills">
			<h3 class="clr2"><span class="lang_pl">WYKSZTAŁCENIE</span><span class="lang_en">EDUCATION</span></h3>
			<div class="skill_list">
			
	<!--=====================-->
			<div class="company_details">
				<h4><span class="lang_pl">Politechnika Warszawska</span><span class="lang_en">Warsaw University of Technology</span><span>Od 2013</span></h4>
				<h6><span class="lang_pl">Elektronika i Telekomunikacja</span><span class="lang_en">Electronics and telecommunication</span></h6>
				<p><span class="lang_pl">Studia pierwszego stopnia na kierunku Elektronika i Telekomunikacja, specjalizacja Teleinformatyka, Politechnika Warszawska (tryb zaoczny)</span>
					<span class="lang_en">First-cycle studies in the field of Electronics and Telecommunications, specialization Teleinformatics, Warsaw University of Technology (extramural)</span></p>
				<p class="cmpny1"></p>
			</div>			
	<!--=====================-->
			<div class="company_details">
				<h4><span class="lang_pl">Politechnika Warszawska</span><span class="lang_en">Warsaw University of Technology</span><span>Od 2008</span></h4>
				<h6><span class="lang_pl">Fizyka Techniczna</span><span class="lang_en">Technical Physics</span></h6>
				<p><span class="lang_pl">Studia pierwszego stopnia na kierunku Fizyka Techniczna, specjalizacja Fizyka Komputerowa, Politechnika Warszawska</span>
					<span class="lang_en">First-cycle studies in the field of Technical Physics, specialization in Computer Physics, Warsaw University of Technology</span></p>
				<p class="cmpny1"></p>
			</div>			
	<!--=====================-->
			<div class="company_details">
				<h4><span class="lang_pl">LVI Liceum Ogólnokształcące</span><span class="lang_en">LVI High School</span><span>2005 - 2008</span></h4>
				<h6><span class="lang_pl">Klasa o profilu matematyczno-fizycznym</span><span class="lang_en">A class with a mathematical-physical profile</span></h6>
				<p><span class="lang_pl">LVI Liceum Ogólnokształcące im. Leona Kruczkowskiego w Warszawie: klasa o profilu matematyczno-fizycznym z rozszerzonym j. rosyjskim</span>
					<span class="lang_en">LVI High School them. Leon Kruczkowski in Warsaw: a class with a mathematical-physical profile with an extended Russian language</span></p>
			</div>			
	<!--=====================-->
	
			</div>
		</div>
		
<!--===========================================================-->

<!--KURSY I CERTYFIKATY================================================-->
		<div class="skills">
			<h3 class="clr2"><span class="lang_pl">KURSY I CERTYFIKATY</span><span class="lang_en">COURSES AND CERTIFICATES</span></h3>
			<div class="skill_list">
				<div class="ul_margines ul_margines_CERT demibold">
					<ul>
						<li><span class="lang_pl">Kurs <b>programowania i analizy zabezpieczeń wewnątrzsieciowych</b> - Pałac Kultury i Nauki</span>
							<span class="lang_en"><b>Programming and analysis course for intra-network security</b> - Palace of Culture and Science</span></li>
						<li><span class="lang_pl">Kurs grafiki komputerowej – <b>Corel Draw/Photoshop</b> – Centrum aktywizacji zawodowej s.c.</span>
							<span class="lang_en">Computer graphics course - <b>Corel Draw / Photoshop</b> - Center for professional activation sc</span></li>
						<li><span class="lang_pl">Kurs IBM Fundamental System Skills in <b>z/OS</b> - IBM</span>
							<span class="lang_en">IBM Fundamental System Skills in <b>z/OS</b> course - IBM</span></li>
						<li><span class="lang_pl">Kurs IBM <b>z/OS JCL and Utilities</b> - IBM</span>
							<span class="lang_en">IBM <b>z/OS JCL and Utilities course</b> - IBM</span></li>
							
						<li><span class="lang_pl">Kurs - <b>Responsive Web Design</b> - Wprowadzenie do projektowania nowoczesnych stron</span>
							<span class="lang_en">Course - <b>Responsive Web Design</b> - Introduction to the design of modern websites</span></li>
						<li><span class="lang_pl">Kurs - Do czego można wykorzystać język <b>Javascript</b></span>
							<span class="lang_en">Course - What can <b>Javascript</b> be used for?</span></li>
							
						<li><span class="lang_pl">Certyfikat <b>ECDL</b> nr. PL-0116993</span>
							<span class="lang_en"><b>ECDL</b> certificate no. EN-0116993</span></li>
							
						<li><span class="lang_pl">Szkolenie <b>Komunikacja i współpraca</b></span>
							<span class="lang_en">Training <b>Communication and cooperation</b></span></li>
						<li><span class="lang_pl">Szkolenie <b>Zarządzanie Ryzykiem Operacyjnym</b></span>
							<span class="lang_en">Training <b>Operational Risk Management</b></span></li>
						<li><span class="lang_pl">Szkolenie <b>Wymagania w zakresie ochrony danych osobowych</b></span>
							<span class="lang_en">Training <b>Requirements for the protection of personal data</b></span></li>
						<li><span class="lang_pl">Szkolenie <b>System Zarządzania Ciągłością Działania (ISO 22301)</b></span>
							<span class="lang_en">Training <b>Business Continuity Management System (ISO 22301)</b></span></li>
						<li><span class="lang_pl">Szkolenie <b>Przeciwdziałania Praniu Brudnych Pieniędzy</b></span>
							<span class="lang_en">Training <b>Anti-Money Laundering Training</b></span></li>
					</ul>
				</div>
			</div>
		</div>
<!--===========================================================-->

<!--DODATKOWE UMIEJĘTNOŚCI================================================-->
		<div class="skills">
			<h3 class="clr2"><span class="lang_pl">DODATKOWE UMIEJĘTNOŚCI</span><span class="lang_en">ADDITIONAL SKILLS</span></h3>
			<div class="skill_list">
				<div class="ul_margines ul_margines_CERT demibold">
					<ul>
						<li><span class="lang_pl">łatwość pracy w grupie i nawiązywania nowych kontaktów</span><span class="lang_en">ease of working in a group and making new contacts</span></li>
						<li><span class="lang_pl">umiejętność analitycznego i abstrakcyjnego myślenia</span><span class="lang_en">the skill of analytical and abstract thinking</span></li>
						<li><span class="lang_pl">szybkie przyswajanie wiedzy i chęć nauki nowych zagadnień</span><span class="lang_en">fast assimilation of knowledge and willingness to learn new issues</span></li>
						<li><span class="lang_pl">dobra organizacja pracy, orientacja w terenie</span><span class="lang_en">good work organization, orientation in the field</span></li>
					</ul>
				</div>
			</div>
		</div>
<!--===========================================================-->

<!--ZAINTERESOWANIA================================================-->
		<div class="skills marg_bott_0">
			<h3 class="clr2"><span class="lang_pl">ZAINTERESOWANIA</span><span class="lang_en">INTERESTS</span></h3>
			<div class="skill_list">
				<div class="ul_margines ul_margines_CERT">
					<span class="lang_pl">IT, podróże, fotografia (timelaps), żeglarstwo (patent żeglarza jachtowego), snowboard, kitesurfing, taniec towarzyski, nurkowanie (PADI - Open Water Diver), paralotniarstwo (świadectwo kwalifikacji pilota paralotni)</span>
						<span class="lang_en">IT, travel, photography (timelaps), sailing (yacht sailor's license), snowboarding, kitesurfing, ballroom dancing, diving (PADI - Open Water Diver), paragliding (paraglider pilot's certificate)</span>
				</div>
			</div>
		</div>
<!--===========================================================-->
		
		
		
		<div class="copywrite">
			<p>© 2015 Curriculum Vitae All Rights Reseverd | Design by <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
		</div>
	</div>
</div>
</body>
</html>
